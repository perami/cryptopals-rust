extern crate cryptopals;

#[test]
fn test_add() {
    assert_eq!(cryptopals::add(2, 3), 5);
}
